// ESLint configuration.

module.exports = {
	"extends": [
		"@studiohyperdrive/eslint-config",
		"@studiohyperdrive/eslint-config/lib/es6.js",
	],

	// Override rules if needed.
	// This is not recommended. Please report issues you encounter with the @studiohyperdrive/eslint-config
	// package in the issue queue: https://bitbucket.org/district01/eslint-config/issues.
};
