import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { DrinksModule } from "@drinks";
import { RoutingModule } from "@routing";
import { SharedModule } from "@shared";

import { AppComponent } from "./app.component";

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		DrinksModule,
		RoutingModule,
		SharedModule,
	],
	exports: [],
	providers: [],
	bootstrap: [AppComponent],
})

export class AppModule {}
