import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { SharedModule } from "@shared";

import { ROUTES } from "./routing.conf";

@NgModule({
	imports: [
		SharedModule,
		RouterModule.forRoot(ROUTES),
	],
	declarations: [],
	providers: [],
	exports: [],
})

export class RoutingModule {}
