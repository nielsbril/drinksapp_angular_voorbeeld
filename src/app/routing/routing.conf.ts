import { Routes } from "@angular/router";

import { DRINKS_ROUTES } from "@drinks";

export const ROUTES: Routes = [
	...DRINKS_ROUTES,
	{
		path: "",
		redirectTo: "/drinks",
		pathMatch: "full",
	},
	{
		path: "**",
		redirectTo: "/drinks",
		pathMatch: "full",
	},
];
