import { Injectable } from "@angular/core";
import { NgRedux } from "@angular-redux/store";

import { AppState } from "./../store.types";
import { InStore } from "./../utils/in-store";

@Injectable()
export class InStoreService {
	constructor(
		private inStore: InStore,
		private ngRedux: NgRedux<AppState>
	) {}

	public getFromState(location: string): any[] {
		const state = this.ngRedux.getState();

		return this.inStore.getFromState(state, location);
	}

	public isInState(location: string, find): boolean {
		const state = this.ngRedux.getState();

		return this.inStore.isInState(state, location, find);
	}
}
