import { combineReducers } from "redux";

import { AppState } from "./store.types";

// Order
import { DrinksReducers, DRINKS_INITIAL_STATE } from "@drinks/store";

export const rootReducer = combineReducers<AppState>({
	...DrinksReducers,
});

export const initialState: AppState = {
	...DRINKS_INITIAL_STATE,
};
