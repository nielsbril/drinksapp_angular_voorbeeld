import * as get from "lodash.get";

export class InStore {
	public getFromState(state: any, location: string): any[] {
		return get(state, location, null);
	}

	public isInState(state: any, location: string, find): boolean {
		const store = get(state, location, false);

		return !!store && !!find ? find(store) !== undefined : false;
	}
}
