export interface Action {
	type: string;
	payload?: any;
	[key: string]: any;
}

import { DrinksState } from "@drinks/store";

export interface RootState {} // tslint:disable-line no-empty-interface

export type AppState = RootState & DrinksState;
