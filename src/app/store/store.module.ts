import { NgModule } from "@angular/core";
import { NgReduxModule, NgRedux, DevToolsExtension } from "@angular-redux/store";

import { AppState } from "./store.types";
import { Services } from "./services";
import { Utils } from "./utils";
import { rootReducer, initialState } from "./store.conf";

import { environment } from "@env";

@NgModule({
	declarations: [],
	imports: [
		NgReduxModule,
	],
	exports: [
		NgReduxModule,
	],
	providers: [
		...Services,
		...Utils,
	],
})

export class StoreModule {
	constructor(
		private devTools: DevToolsExtension,
		private ngRedux: NgRedux<AppState>
	) {
		const enhancers = environment.reduxDevTools && this.devTools.isEnabled() ? [this.devTools.enhancer()] : [];

		this.ngRedux.configureStore(rootReducer, initialState, [], enhancers);
	}
}
