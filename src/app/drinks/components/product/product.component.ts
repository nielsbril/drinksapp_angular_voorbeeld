import { Component, Input, Output, EventEmitter } from "@angular/core";

import { ProductsActionCreator } from "@drinks/store/products/products.actioncreator";

import { Product } from "@types";

@Component({
	selector: "da-product",
	templateUrl: "./product.component.html",
})

export class ProductComponent {
	@Input() public product: Product;

	constructor(
		private productActions: ProductsActionCreator
	) {}

	/* Events */
	public increment(): void {
		this.product.quantity++;
		this.updateOrder();
	}

	public decrement(): void {
		if (!this.product.quantity) {
			return;
		}

		this.product.quantity--;
		this.updateOrder();
	}

	private updateOrder(): void {
		this.productActions.updateProducts(this.product);
	}
}
