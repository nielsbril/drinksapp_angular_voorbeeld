import { Component, OnInit, OnDestroy } from "@angular/core";

import { OrderActionCreator } from "@drinks/store/order/order.actioncreator";
import { ProductsService } from "@drinks/services";

import { AppState } from "@store";
import { NgRedux } from "@angular-redux/store";
import { Product } from "@types";
import { Subscription } from "rxjs/Subscription";

import * as filter from "lodash.filter";
import * as sortBy from "lodash.sortby";

@Component({
	selector: "da-products",
	templateUrl: "./products.container.html",
})

export class ProductsComponent implements OnInit, OnDestroy {
	public products: Product[] = [];
	public query: string = "";

	private allProducts: Product[] = [];
	private subscriptions: Subscription[] = [];

	constructor(
		private ngRedux: NgRedux<AppState>,
		private orderActions: OrderActionCreator,
		private productsService: ProductsService
	) {}

	/* Lifecycle */
	public ngOnInit(): void {
		this.productsService.getProducts();

		this.subscriptions.push(
			this.ngRedux.select(["products"]).subscribe((products: Product[]) => {
				// Sort products
				products = sortBy(products, ["name"]);

				// Update order
				this.orderActions.updateOrderProducts(products);

				// Filter products
				this.filter(products);
			})
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}

	/* Events */
	public filter(products?: Product[]): void {
		if (products) {
			this.allProducts = products;
		}

		this.products = filter(this.allProducts, (product: Product) => {
			return product.name.toLowerCase().includes(this.query.toLowerCase());
		});
	}
}
