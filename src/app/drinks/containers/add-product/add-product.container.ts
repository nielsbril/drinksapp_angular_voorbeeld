import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { ProductsService } from "@drinks/services";

@Component({
	selector: "da-add-product",
	templateUrl: "./add-product.container.html",
})

export class AddProductComponent implements OnInit {
	public form: FormGroup;

	constructor(
		private fb: FormBuilder,
		private productsService: ProductsService
	) {}

	/* Lifecycle */
	public ngOnInit(): void {
		this.form = this.fb.group({
			name: ["", [ Validators.required ]],
		});
	}

	/* Events */
	public submit(value: any) {
		this.productsService.addProduct(value.name)
			.then(() => {
				this.form.reset();
			})
			.catch(() => {
				this.form.reset();
			});
	}
}
