import { Component } from "@angular/core";

import { OrderActionCreator } from "@drinks/store/order/order.actioncreator";

import * as debounce from "lodash.debounce";

@Component({
	selector: "da-intro",
	templateUrl: "./intro.container.html",
})

export class IntroComponent {
	public username: string = "";

	public setUsername = debounce(() => {
		this.orderActions.updateOrderUsername(this.username);
	}, 500);

	constructor(
		private orderActions: OrderActionCreator
	) {}
}
