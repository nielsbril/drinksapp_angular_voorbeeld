import { Component, OnInit, OnDestroy } from "@angular/core";

import { OrderActionCreator } from "@drinks/store/order/order.actioncreator";
import { OrderService } from "@drinks/services";
import { ProductsActionCreator } from "@drinks/store/products/products.actioncreator";

import { AppState } from "@store";
import { NgRedux } from "@angular-redux/store";
import { Order } from "@types";
import { Subscription } from "rxjs/Subscription";

@Component({
	selector: "da-order",
	templateUrl: "./order.container.html",
})

export class OrderComponent implements OnInit, OnDestroy {
	public order: Order;

	private subscriptions: Subscription[] = [];

	constructor(
		private ngRedux: NgRedux<AppState>,
		private orderActions: OrderActionCreator,
		private orderService: OrderService,
		private productsActions: ProductsActionCreator
	) {}

	/* Lifecycle */
	public ngOnInit(): void {
		this.subscriptions.push(
			this.ngRedux.select(["order"]).subscribe((order: Order) => {
				this.order = order;
			})
		);
	}

	public ngOnDestroy(): void {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}

	/* Events */
	public sendOrder(): void {
		this.orderService.sendOrder(this.order)
			.then(() => {
				this.productsActions.resetProducts();
			})
			.catch(() => {
				this.productsActions.resetProducts();
			});
	}
}
