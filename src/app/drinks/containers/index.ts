import { AddProductComponent } from "./add-product/add-product.container";
import { IntroComponent } from "./intro/intro.container";
import { OrderComponent } from "./order/order.container";
import { ProductsComponent } from "./products/products.container";

const Containers: any[] = [
	AddProductComponent,
	IntroComponent,
	OrderComponent,
	ProductsComponent,
];

export {
	Containers,

	AddProductComponent,
	IntroComponent,
	OrderComponent,
	ProductsComponent,
};
