import { Routes } from "@angular/router";

import { DrinksPageComponent } from "./pages/drinks/drinks.page";

export const DRINKS_ROUTES: Routes = [
	{
		path: "drinks",
		component: DrinksPageComponent,
	},
];
