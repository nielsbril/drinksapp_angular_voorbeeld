import { DrinksPageComponent } from "./drinks/drinks.page";

const Pages: any[] = [
	DrinksPageComponent,
];

export {
	Pages,

	DrinksPageComponent,
};
