import { NgModule } from "@angular/core";

import { SharedModule } from "@shared";

import { Components } from "./components";
import { Containers } from "./containers";
import { Pages } from "./pages";
import { Providers } from "./store";
import { Services } from "./services";

@NgModule({
	declarations: [
		...Components,
		...Containers,
		...Pages,
	],
	imports: [
		SharedModule,
	],
	exports: [
		...Components,
		...Containers,
		...Pages,
	],
	providers: [
		...Providers,
		...Services,
	],
})

export class DrinksModule {}
