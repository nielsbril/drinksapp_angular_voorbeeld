import { Action } from "@store/store.types";
import * as actionTypes from "./products.actiontypes";

import { ProductsService } from "@drinks/services";

import { Product } from "@types";

export function addProduct(product: Product): Action {
	return {
		type: actionTypes.PRODUCTS_ADD,
		payload: product,
	};
}

export function resetProducts(): Action {
	return {
		type: actionTypes.PRODUCTS_RESET,
	};
}

export function setProducts(products: Product[]): Action {
	return {
		type: actionTypes.PRODUCTS_SET,
		payload: products,
	};
}

export function updateProducts(product: Product): Action {
	return {
		type: actionTypes.PRODUCTS_UPDATE,
		payload: product,
	};
}
