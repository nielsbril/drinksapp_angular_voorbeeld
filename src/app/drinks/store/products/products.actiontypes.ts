export const PRODUCTS_TYPE = "PRODUCTS";

export const PRODUCTS_ADD = `${PRODUCTS_TYPE}/ADD`;
export const PRODUCTS_RESET = `${PRODUCTS_TYPE}/RESET`;
export const PRODUCTS_SET = `${PRODUCTS_TYPE}/SET`;
export const PRODUCTS_UPDATE = `${PRODUCTS_TYPE}/UPDATE`;
