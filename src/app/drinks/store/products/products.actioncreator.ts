import { Injectable } from "@angular/core";
import { NgRedux } from "@angular-redux/store";
import { bindActionCreators } from "redux";

import { DrinksState } from "./../store.types";
import * as actions from "./products.actions";

@Injectable()
export class ProductsActionCreator {
	public addProduct: any;
	public resetProducts: any;
	public setProducts: any;
	public updateProducts: any;

	constructor(
		private ngRedux: NgRedux<DrinksState>
	) {
		this.addProduct = bindActionCreators(actions.addProduct, this.ngRedux.dispatch);
		this.resetProducts = bindActionCreators(actions.resetProducts, this.ngRedux.dispatch);
		this.setProducts = bindActionCreators(actions.setProducts, this.ngRedux.dispatch);
		this.updateProducts = bindActionCreators(actions.updateProducts, this.ngRedux.dispatch);
	}
}
