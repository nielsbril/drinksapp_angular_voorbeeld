import { Action } from "@store/store.types";
import { Reducer } from "redux";

import { Product } from "@shared";
import { PRODUCTS_INITIAL_STATE } from "./products.initial-state";
import {
	PRODUCTS_ADD,
	PRODUCTS_RESET,
	PRODUCTS_SET,
	PRODUCTS_UPDATE,
} from "./products.actiontypes";

const reducer: Reducer<Product[]> = (
	state: Product[] = PRODUCTS_INITIAL_STATE,
	action: Action
): Product[] => {
	if (action.type === PRODUCTS_SET) {
		return [
			...action.payload,
		];
	}

	if (action.type === PRODUCTS_RESET) {
		return state.reduce((acc: Product[], product: Product) => {
			acc.push({
				name: product.name,
				quantity: 0,
			});

			return acc;
		}, []);
	}

	if (action.type === PRODUCTS_ADD) {
		return [
			...state,
			action.payload,
		];
	}

	if (action.type === PRODUCTS_UPDATE) {
		return state.reduce((acc: Product[], product: Product) => {
			if (product.name === action.payload.name) {
				acc.push(action.payload);
			} else {
				acc.push(product);
			}

			return acc;
		}, []);
	}

	return state;
};

export const productsReducer = reducer;
