import { Action } from "@store/store.types";
import { Reducer } from "redux";

import {
	Order,
	Product,
} from "@shared";
import { ORDER_INITIAL_STATE } from "./order.initial-state";
import {
	ORDER_UPDATE_USERNAME,
	ORDER_UPDATE_PRODUCTS,
} from "./order.actiontypes";

const reducer: Reducer<Order> = (
	state: Order = ORDER_INITIAL_STATE,
	action: Action
): Order => {
	if (action.type === ORDER_UPDATE_USERNAME) {
		return {
			...state,
			username: action.payload,
		};
	}

	if (action.type === ORDER_UPDATE_PRODUCTS) {
		return {
			...state,
			products: action.payload.reduce((acc: Product[], product: Product) => {
				if (product.quantity) {
					acc.push(product);
				}

				return acc;
			}, []),
		};
	}

	return state;
};

export const orderReducer = reducer;
