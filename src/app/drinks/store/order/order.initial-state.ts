import { Order } from "@types";

export const ORDER_INITIAL_STATE: Order = {
	username: "",
	products: [],
};
