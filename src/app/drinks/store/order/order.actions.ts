import { Action } from "@store/store.types";
import * as actionTypes from "./order.actiontypes";

import {
	Order,
	Product,
} from "@types";

export function updateOrderUsername(username: string): Action {
	return {
		type: actionTypes.ORDER_UPDATE_USERNAME,
		payload: username,
	};
}

export function updateOrderProducts(product: Product): Action {
	return {
		type: actionTypes.ORDER_UPDATE_PRODUCTS,
		payload: product,
	};
}
