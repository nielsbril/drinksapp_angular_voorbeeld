import { Injectable } from "@angular/core";
import { NgRedux } from "@angular-redux/store";
import { bindActionCreators } from "redux";

import { DrinksState } from "./../store.types";
import * as actions from "./order.actions";

@Injectable()
export class OrderActionCreator {
	public updateOrderUsername: any;
	public updateOrderProducts: any;

	constructor(
		private ngRedux: NgRedux<DrinksState>
	) {
		this.updateOrderUsername = bindActionCreators(actions.updateOrderUsername, this.ngRedux.dispatch);
		this.updateOrderProducts = bindActionCreators(actions.updateOrderProducts, this.ngRedux.dispatch);
	}
}
