import { DrinksState } from "./store.types";

// Order
import { OrderActionCreator } from "./order/order.actioncreator";
import { ORDER_INITIAL_STATE } from "./order/order.initial-state";
import { orderReducer } from "./order/order.reducer";

// Order
import { ProductsActionCreator } from "./products/products.actioncreator";
import { PRODUCTS_INITIAL_STATE } from "./products/products.initial-state";
import { productsReducer } from "./products/products.reducer";

export { DrinksState } from "./store.types";

export const Providers: any[] = [
	OrderActionCreator,
	ProductsActionCreator,
];

export const DrinksReducers = {
	order: orderReducer,
	products: productsReducer,
};

export const DRINKS_INITIAL_STATE: DrinksState = {
	order: ORDER_INITIAL_STATE,
	products: PRODUCTS_INITIAL_STATE,
};
