import {
	Order,
	Product,
} from "@types";

export interface DrinksState {
	order: Order;
	products: Product[];
}
