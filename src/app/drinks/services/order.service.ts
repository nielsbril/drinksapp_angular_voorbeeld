import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { ProductsActionCreator } from "@drinks/store/products/products.actioncreator";

import { Order } from "@types";

@Injectable()
export class OrderService {
	constructor(
		private httpClient: HttpClient,
		private productsActions: ProductsActionCreator
	) { }

	public sendOrder(order: Order): Promise<any> {
		return this.httpClient.post("http://imd.district01.be/order", order).toPromise();
	}
}
