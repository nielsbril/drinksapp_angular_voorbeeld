import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { ProductsActionCreator } from "@drinks/store/products/products.actioncreator";

import { Product } from "@types";

@Injectable()
export class ProductsService {
	constructor(
		private httpClient: HttpClient,
		private productsActions: ProductsActionCreator
	) { }

	public getProducts(): Promise<any> {
		return this.httpClient.get("http://imd.district01.be/product").toPromise()
			.then((response: any[]) => {
				const products: Product[] = response.reduce((acc: Product[], product: any) => {
					acc.push({
						name: product.name,
						quantity: 0,
					});

					return acc;
				}, []);

				this.productsActions.setProducts(products);
			})
			.catch((err: any) => {
				// Error handling
			});
	}

	public addProduct(name: string): Promise<any> {
		return this.httpClient.post("http://imd.district01.be/product", { name }).toPromise()
			.then((res: any) => {
				this.productsActions.addProduct({
					name,
					quantity: 0,
				});
			})
			.catch((err: any) => {
				// Error handling
			});
	}
}
