import { OrderService } from "./order.service";
import { ProductsService } from "./products.service";

const Services: any[] = [
	OrderService,
	ProductsService,
];

export {
	Services,

	OrderService,
	ProductsService,
};
