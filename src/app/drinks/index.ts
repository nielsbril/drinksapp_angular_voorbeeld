export { DrinksModule } from "./drinks.module";
export { DRINKS_ROUTES } from "./drinks.routes";

export {
	ProductComponent,
} from "./components";
export {
	AddProductComponent,
	IntroComponent,
	OrderComponent,
	ProductsComponent,
} from "./containers";
export {
	DrinksPageComponent,
} from "./pages";
export {
	OrderService,
	ProductsService,
} from "./services";
