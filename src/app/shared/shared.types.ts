export interface Product {
	name: string;
	quantity: number;
}

export interface Order {
	username: string;
	products: Product[];
}
